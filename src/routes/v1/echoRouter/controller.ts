import { Request, Response } from 'express';
import { EchoService } from 'src/services/v1/echoService/service';

export class EchoController {
  private service: EchoService;
  constructor(service: EchoService) {
    this.service = service;
  }

  public async postEcho(req: Request, res: Response) {
    const { body } = req;

    const response = await this.service.echo(body);

    res.json(response);
  }
}
