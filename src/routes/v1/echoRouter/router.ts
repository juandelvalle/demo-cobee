import { Router } from 'express';
import { EchoService } from 'src/services/v1/echoService/service';
import { EchoController } from './controller';

export class EchoRouter {
  private controller: EchoController;

  constructor(service: EchoService) {
    this.controller = new EchoController(service);
  }

  public configureApplication(appRouter: Router, basePath = 'echo', version = 'v1') {
    const router = Router();
    router.post('', this.controller.postEcho.bind(this.controller));

    appRouter.use(`/${version}/${basePath}`, router);
  }
}
