import { Config } from './config';
import { Server } from './server';

const server = new Server();

const port = Config.getInstance().getConfig('SERVER_PORT', '8080');
server.listen(port);
