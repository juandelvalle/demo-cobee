import * as dotenv from 'dotenv';

export class Config {
  private static instance: Config;
  public static getInstance(): Config {
    if (!Config.instance) {
      Config.instance = new Config();
    }

    return Config.instance;
  }

  private config: dotenv.DotenvConfigOutput;

  private constructor() {
    this.config = dotenv.config();
  }

  public getConfig(variableName: string, defaultValue = ''): string {
    return process.env[variableName] || defaultValue;
  }
}
