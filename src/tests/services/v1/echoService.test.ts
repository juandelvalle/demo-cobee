import { expect } from 'chai';
import { EchoService } from '../../../services/v1/echoService';
import { dummyData } from '../../data';

const echoService = new EchoService();

describe('EchoService tests', () => {
  it('checking echo service', async () => {
    const response = await echoService.echo(dummyData);
    expect(response).to.deep.equals(dummyData);
  });
});
