import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import { Server } from '../server';

const server = new Server();
chai.use(chaiHttp);

describe('API Server tests', () => {
  it('starting server', (done) => {
    chai
      .request(server.getApp())
      .get('/')
      .end((err, res) => {
        if (err) done(err);
        expect(res).to.have.status(404);
        done();
      });
  });
});
