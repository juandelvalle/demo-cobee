import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import { Server } from '../../../server';
import { dummyData } from '../../data';

const server = new Server();
chai.use(chaiHttp);

describe('Echo Router tests', () => {
  it('testing /v1/echo endpoint', (done) => {
    chai
      .request(server.getApp())
      .post('/api/v1/echo')
      .send({ ...dummyData })
      .end((err, res) => {
        if (err) done(err);
        expect(res).to.have.status(200);
        expect(res.body).to.deep.equals(dummyData);
        done();
      });
  });
});
