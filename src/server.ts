import express, { Application } from 'express';
import * as bodyParser from 'body-parser';
import helmet from 'helmet';
import cors from 'cors';
import compression from 'compression';

import { EchoRouter } from './routes/v1/echoRouter';
import { EchoService } from './services/v1/echoService';

export class Server {
  private app: Application;

  constructor() {
    this.app = express();
    this.configureApplication();
    this.configureRoutes();
  }

  public getApp() {
    return this.app;
  }

  public listen(port: string): void {
    const server = this.app.listen(port, () => {
      return console.log(`Server is listening on port ${port}`);
    });

    server.on('error', (e: NodeJS.ErrnoException) => {
      if (e.code === 'EADDRINUSE') {
        console.log('Address in use, retrying...');
        setTimeout(() => {
          this.listen(port);
        }, 5000);
      }
    });
  }

  private configureApplication(): void {
    this.app.use(compression());
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json());
    this.app.use(helmet());
    this.app.use(cors());
  }

  private configureRoutes(): void {
    const apiRouter = express.Router();

    const echoRouter = new EchoRouter(new EchoService());
    echoRouter.configureApplication(apiRouter);

    this.app.use('/api', apiRouter);
  }
}
